import React, { useState, useEffect } from 'react'
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import PropTypes, { func } from 'prop-types';
import { AsyncParser, parseAsync } from 'json2csv';
import {
  MuiThemeProvider,
  createMuiTheme,
  TablePagination,
  Dialog,
  Button,
  CardActions,
  Checkbox,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
  Grid,
  Snackbar
} from '@material-ui/core';
import UserModel from '../../models/UserModel'
import MaterialTable from 'material-table';
import { LensOutlined } from '@material-ui/icons';
import MuiAlert from '@material-ui/lab/Alert';
import { validateNumeric, validateNumericNoDecimal } from '../../common/validators';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 800
  },
  statusContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  status: {
    marginRight: theme.spacing(1)
  },
  actions: {
    justifyContent: 'flex-end'
  }

}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const WarehouseConfirm = (props) => {
  const [params, setParams] = useState({
    from: '2020-10-21T06:00',
    to: '2020-10-21T15:00',
    status: 0
  })
  const [selectedBatch, setSelectedBatch] = useState({});
  const [isLoad, setIsLoad] = useState(false)
  const [boolVal, setBoolVal] = useState('')
  const [openData, setOpenData] = useState({
    openSuccess: false,
    openError: false
  });
  const classes = useStyles();
  const { className, ...rest } = props;
  const [batches, setBatches] = useState([])
  // { id: 2, batch_name: "batch_1603439319182", created_at: "2020-10-23T07:48:39.184Z", updated_at: "2020-10-23T07:48:39.184Z", warehouse_id: 1 }
  var [data, setData] = useState([])
  const columns = [
    { title: 'SKU', field: 'sku_name', editable: 'never' },
    { title: 'Time', field: 'procurement_date', editable: 'never', filtering: false }, //This is the procurement time
    {
      title: 'Procurement Cost',
      field: 'procure_price',
      editable: 'never',
      filtering: true,
    },
    {
      title: 'Warehouse Quantity',
      field: 'wh_qtty',
      editable: 'never',
      filtering: false
    },
    // { title: 'Required Purchase Quantity', field: 'reqd_purchase_qtty', editable: 'never', filtering: false },
    { title: 'Procured Quantity', field: 'qty', editable: 'never', filtering: false },
    // {
    //   title: 'Fulfilled', field: 'fulfilled', name: 'xyz', render: rowData => {
    //     return (
    //       // (rowData.is_verify) ?
    //       //   (<Checkbox disabled={true} />)
    //       //   :
    //       (<Checkbox value="true" checked={rowData.fulfilled} disabled={rowData.is_procure ? false : true} onChange={async (evt, val) => {

    //         let newData = { sku_name: rowData.sku_name, is_procure: rowData.is_procure, procurement_id: rowData.procurement_id, procure_price: rowData.procure_price, qty: rowData.qty, wh_qtty: rowData.wh_qtty, fulfilled: val, act_purchase: rowData.qty, total_purchase_cost: rowData.total_purchase_cost }
    //         console.log('new daaata', newData)
    //         const dataUpdate = [...data];
    //         const index = rowData.tableData.id;
    //         dataUpdate[index] = newData;
    //         setData([...dataUpdate]);
    //       }} />)
    //     )
    //   }
    //   , filtering: false
    // },
    {
      title: 'Actual Procured Quantity',
      field: 'act_purchase',
      editable: 'never',
      filtering: false,

      render: rowData => <TextField
        value={rowData.act_purchase}
        // disabled={rowData.is_procure ? false : true}
        type="number"
        onChange={async (evt) => {
          if (validateNumericNoDecimal(parseFloat(evt.target.value)) || !evt.target.value) {
            console.log('rowDaata', rowData)
            let newData = { procurement_id: rowData.id, procurement_date: rowData.procurement_date, sku_name: rowData.sku_name, is_verify: rowData.is_verify, procurement_id: rowData.procurement_id, sku_id: rowData.sku_id, procure_price: rowData.procure_price, qty: rowData.qty, wh_qtty: rowData.wh_qtty, fulfilled: rowData.fulfilled, act_purchase: evt.target.value, total_purchase_cost: rowData.total_purchase_cost }
            const dataUpdate = [...data];
            const index = rowData.tableData.id;
            dataUpdate[index] = newData
            setData([...dataUpdate])
          }
        }}
      />
    },

  ]

  useEffect(() => {
    let tempArr = []
    UserModel.getInstance().getRangeOrdersManager(
      // { "from": params.from, "to": params.to },
      null,
      async data => {
        console.log('procurement data', data)
        await data.forEach(obj => {
          console.log(obj)
          tempArr.push({
            sku_id: obj.sku_id,
            sku_name: obj.name,
            // procure_price: obj.order_qty,
            procure_price: obj.procure_price,
            wh_qtty: obj.stock,
            // reqd_purchase_qtty: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
            fulfilled: false,
            act_purchase: obj.verify_procure_qty ? obj.verify_procure_qty : '',
            total_purchase_cost: '',
            procurement_id: obj.id,
            qty: obj.procure_qty,
            is_verify: obj.verify_procure,
            is_procure: obj.is_procure,
            procurement_date: obj.procurement_date
          })
        })
        // console.log('temp arr', tempArr)
        setData(tempArr)
        setIsLoad(false)
      },
      err => {
        setIsLoad(false)
        // alert(err)
        console.log('err', err)
      }
    )
  }, [])

  const handleChange = (event) => {
    setIsLoad(true)
    setData([])
    console.log('value', event.target.value)
    setSelectedBatch(event.target.value);
    let tempArr = [];
    UserModel.getInstance().getManagerSpecificBatch(
      null,
      async data => {
        console.log('procurement data', data)
        await data.forEach(obj => {
          console.log(obj)
          tempArr.push({
            sku_name: obj.sku_name,
            procure_price: obj.order_qty,
            wh_qtty: obj.stock,
            // reqd_purchase_qtty: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
            fulfilled: false,
            act_purchase: obj.verify_procure_qty ? obj.verify_procure_qty : '',
            total_purchase_cost: '',
            procurement_id: obj.id,
            qty: obj.procure_qty,
            is_verify: obj.verify_procure,
            is_procure: obj.is_procure,
            procurement_date: obj.procurement_date
          })
        })
        // console.log('temp arr', tempArr)
        setData(tempArr)
        setIsLoad(false)
      },
      err => {
        setIsLoad(false)
        alert(err)
        console.log('err', err)
      }
    )
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenData({ openSuccess: false, openError: false });
  };

  const handleSubmit = () => {
    let obj = [];
    let blankQty = false;
    data.forEach((row) => {
      // console.log(row)
      if (row.act_purchase !== "") {
        obj.push({ id: row.procurement_id, qty: row.act_purchase })
      }
    })
    let newObj = { items: obj }
    console.log(obj)
    UserModel.getInstance().procurementConfirmation(
      { items: obj },
      async (succ) => {
        await setOpenData({ openSuccess: true });
        console.log(succ)
        window.location.reload();
      },
      async (err) => {
        setOpenData({ openError: true, openSuccess: false });
        console.log(err)
      }
    )

  }

  const dateHandler = async (event) => {
    console.log('evnt ', event.target.value)
    params[event.target.name] = event.target.value
    await setParams({
      ...params,
      [event.target.name]: event.target.value
    })
    console.log('params ', params)
    let from = params.from.slice(0, 10) + ' ' + params.from.slice(11, 16) + ':00';
    let to = params.to.slice(0, 10) + ' ' + params.to.slice(11, 16) + ':00';

    console.log('from', from)
    console.log('to', to)
  }

  const handleClick = async (event) => {
    let tempArr = []
    UserModel.getInstance().getRangeOrdersManager(
      // { "from": params.from, "to": params.to },
      null,
      async data => {
        console.log('procurement data', data)
        await data.forEach(obj => {
          console.log(obj)
          tempArr.push({
            sku_id: obj.sku_id,
            sku_name: obj.name,
            // procure_price: obj.order_qty,
            procure_price: obj.procure_price,
            wh_qtty: obj.stock,
            // reqd_purchase_qtty: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
            fulfilled: false,
            act_purchase: obj.verify_procure_qty ? obj.verify_procure_qty : '',
            total_purchase_cost: '',
            procurement_id: obj.id,
            qty: obj.procure_qty,
            is_verify: obj.verify_procure,
            is_procure: obj.is_procure,
            procurement_date: obj.procurement_date
          })
        })
        // console.log('temp arr', tempArr)
        setData(tempArr)
        setIsLoad(false)
      },
      err => {
        setIsLoad(false)
        alert(err)
        console.log('err', err)
      }
    )
  }

  return (
    <div>
      {/* <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-helper-label">Batch</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"

          value={selectedBatch}
          onChange={handleChange}
        >
          {
            batches.map((val) => {
              return <MenuItem value={val}>{val.batch_name}</MenuItem>
            })
          }
        </Select>
        <FormHelperText style={{ margin: 10 }}>Select a Batch Id</FormHelperText>
      </FormControl> */}
      {/* <div style={{ marginTop: 20 }} >
        <TextField
          id="datetime-local"
          label="Select From"
          type="datetime-local"
          name='from'
          defaultValue={params.from}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={dateHandler}
        />
        <TextField
          id="datetime-local"
          label="Select To"
          type="datetime-local"
          name='to'
          defaultValue={params.to}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          style={{ marginLeft: 40 }}
          onChange={dateHandler}
        />
      </div>
      <Button style={{ margin: 10 }} onClick={handleClick}>Submit</Button> */}
      <MaterialTable
        title="Procurement Confirmation"
        columns={columns}
        data={data}
        isLoading={isLoad}
        // isLoading={isLoader}
        className={clsx(classes.root, className)}
        // onFilterChange={filterChange}

        options={{
          paging: false,
          filtering: true,
          // exportButton: true,
          // selection: true,

        }}
      ></MaterialTable>
      <Snackbar
        open={openData.openSuccess}
        autoHideDuration={3000}
        onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          procurement Data Successfully updated
              </Alert>
      </Snackbar>
      <Snackbar
        open={openData.openError}
        autoHideDuration={3000}
        onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          Error while updating Data
              </Alert>
      </Snackbar>
      <CardActions>
        <Button color="primary" variant="contained" onClick={handleSubmit} >
          Confirm Procurement
            </Button>
      </CardActions>
    </div>
  )
}

WarehouseConfirm.propTypes = {
  className: PropTypes.string
}

export default WarehouseConfirm
