import React, { useState, useEffect } from 'react'
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import PropTypes, { func } from 'prop-types';
import {
    Button,
    CardActions,
    Checkbox,
    TextField,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    FormHelperText,
    Card,
    Snackbar
} from '@material-ui/core';
import MaterialTable from 'material-table';
import UserModel from '../../models/UserModel'

const useStyles = makeStyles(theme => ({
    root: {},
    content: {
        padding: 0
    },
    inner: {
        minWidth: 800
    },
    statusContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    status: {
        marginRight: theme.spacing(1)
    },
    actions: {
        justifyContent: 'flex-end'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl:{
        margin: theme.spacing(1),
        minWidth: 200,
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
}));



const WhLoadingFlags = (props) => {
    const [boolVal, setBoolVal] = useState('')
    const [routes,setRoutes] = useState([]);
    const [isLoad, setIsLoad] = useState(false)
    const classes = useStyles();
    const { className, ...rest } = props;
    var [data, setData] = useState([
        // { sku: "blabla", route: "Route 4", timestamp: "21/10/20", wh_qtty: "8", loaded_qtty: 8, qtty_reqd: 8, fulfilled: false, act_loaded: "actual purchase", total_purchase_cost: 40, picker: "Salman" },
        // { sku: "blabla2", route: "Route 7", timestamp: "20/10/20", wh_qtty: "50", loaded_qtty: 44, qtty_reqd: 50, fulfilled: false, act_loaded: "actual purchase2", total_purchase_cost: 30, picker: "Hasnain" }
    ])
    const [batches, setBatches] = useState([])
    const [params,setParams] = useState({
        selectedBatch:null,
        selectedRoute:null   
       }
    )
    const columns = [
        { title: 'SKU', field: 'sku', editable: 'never' },
        { title: 'Quantity Required (for route)', field: 'route_qtty', editable: 'never', filtering: false },
        { title: 'Loaded Quantity', field: 'loaded_qtty', editable: 'never', filtering: false },
        {
            title: 'Fulfilled', field: 'fulfilled', render: rowData => <Checkbox value="true" onChange={async (evt, val) => {
                let newData = { sku: rowData.sku, qtty: rowData.qtty, loaded_qtty: rowData.loaded_qtty, wh_qtty: rowData.wh_qtty, qtty_reqd: rowData.qtty_reqd, fulfilled: val, act_loaded: rowData.act_loaded, total_purchase_cost: rowData.total_purchase_cost }
                console.log('new daaata', newData)
                const dataUpdate = [...data];
                const index = rowData.tableData.id;
                dataUpdate[index] = newData;
                setData([...dataUpdate]);
                // rowData.fulfilled = val
                // let dat = {
                //     ...data[rowData.tableData.id],
                //     fulfilled: val
                // };
                // setData(dat)
                //console.log(data)
            }} />, filtering: false
        },
        {
            title: 'Actual Loaded Quantity',
            field: 'act_loaded',
            editable: 'never',
            filtering: false,
            render: rowData => (rowData.fulfilled) ? (<TextField
                value={rowData.qtty_reqd}
                disabled={true}
            />) : (<TextField

                disabled={false}
                onChange={async (evt) => {
                    console.log('rowDaata', rowData)
                    let newData = { sku: rowData.sku, qtty: rowData.qtty, loaded_qtty: rowData.loaded_qtty, wh_qtty: rowData.wh_qtty, qtty_reqd: rowData.qtty_reqd, fulfilled: rowData.fulfilled, act_loaded: evt.target.value, total_purchase_cost: rowData.total_purchase_cost }
                    const dataUpdate = [...data];
                    const index = rowData.tableData.id;
                    dataUpdate[index] = newData
                    setData([...dataUpdate])
                }}
            />)
        },
        { title: 'Picker Name', field: 'picker', editable: 'never', filtering: false },
    ]

    useEffect(() => {
        UserModel.getInstance().getBatchesManager(
            succ => {
              console.log('succ batches', succ)
              setBatches(succ)
            },
            err => {
              console.log('err batches', err)
            }
          )
    }, [])

    const handleChange = async (event) => {
        setIsLoad(true)
        setData([])
        console.log('value', event.target.value)
        console.log('name',event.target.name)
        await setParams({
          ...params,
          [event.target.name] : event.target.value
        })
        if(event.target.name == 'selectedBatch'){
         
        console.log('if handle change')
             setParams({
              ...params,
              [event.target.name] : event.target.value,
              selectedRoute:null
            })
          
          UserModel.getInstance().getSorterRoute(
            event.target.value.id,
             async data => {
             console.log('data routes',data)
             setRoutes(data);
            },
            err => {
              alert(err)
              console.log('err',err)
            }
          )
        }
      
        // setSelectedBatch(event.target.value);
        // let tempArr = [];
        // UserModel.getInstance().getSorterConfirmation(
        //   event.target.value.id,
        //   20,
        //   async data => {
        //     console.log('sorter data',data)
        //     await data.forEach(obj => {
        //       // {sku_id:1, sku_name: 'test1', date:'12/4/20', route_qtty: 10,  sorted_qtty:10 , fulfilled:false, act_sorted_qtty:'', sorter_name:'kumail'}
                    
        //             tempArr.push({
        //               // obj
        //               sku_id:obj.sku_id,
        //               sku_name: obj.name,
        //               date: obj.order_qty,
        //               route_qtty: obj.stock,
        //               reqd_purchase_qtty: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
        //               fulfilled: true,
        //               act_picked: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
        //               total_purchase_cost: '',
        //               procurement_id: obj.id,
        //               qty: obj.qty,
        //               is_verify: obj.is_verify
        //             })
        //           })
        //   },
        //   err => {
        //     console.log('err',err)
        //   }
        // )
      
        // UserModel.getInstance().getManagerSpecificBatch(
        //   event.target.value.id,
        //   async data => {
        //     console.log('procurement data', data)
        //     await data.forEach(obj => {
        //       console.log(obj)
        //       tempArr.push({
        //         // obj
        //         sku_name: obj.sku_name,
        //         qtty: obj.order_qty,
        //         wh_qtty: obj.stock,
        //         reqd_purchase_qtty: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
        //         fulfilled: true,
        //         act_picked: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
        //         total_purchase_cost: '',
        //         procurement_id: obj.id,
        //         qty: obj.qty,
        //         is_verify: obj.is_verify
        //       })
        //     })
        //     // console.log('temp arr', tempArr)
        //     setData(tempArr)
        //     setIsLoad(false)
        //   },
        //   err => {
        //     setIsLoad(false)
        //     console.log('err', err)
        //   }
        // )
      };

    const handleSubmit = () => {
        console.log('handle submit')
        console.log('procurement data', data)
    }

    return (
        <div>
            <div style={{ margin: 20 }} >
            <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-helper-label">Batch</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          name='selectedBatch'
          value={params.selectedBatch}
          onChange={handleChange}
        >
          {
            batches.map((val) => {
              return <MenuItem value={val}>{val.batch_name}</MenuItem>
            })
          }
        </Select>
        <FormHelperText style={{ margin: 10 }}>Select a Batch Id</FormHelperText>
      </FormControl>

      <FormControl className={classes.formControl} style={{ marginLeft: 30 }}>
        <InputLabel id="demo-simple-select-helper-label">Route</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={params.selectedRoute}
          onChange={handleChange}
          name='selectedRoute'
        >
          {
            routes.map((val) => {
              return <MenuItem value={val}>{val.name}</MenuItem>
            })
          }
        </Select>
        <FormHelperText style={{ margin: 10 }}>Select a Route Id</FormHelperText>
      </FormControl>
      <FormControl className={classes.formControl} style={{ marginLeft: 30 }}>
      <Button color="primary" variant="contained"  >
          Get Loading Data
      </Button>
      </FormControl>
            </div>

                <MaterialTable
                    title="Loading Confirmation"
                    columns={columns}
                    data={data}
                    // isLoading={isLoader}
                    className={clsx(classes.root, className)}
                    // onFilterChange={filterChange}

                    options={{
                        paging: false,
                        exportButton: true,
                        filtering: true,
                        //   selection: true,

                    }}
                ></MaterialTable>

            <CardActions>
                <Button color="primary" variant="contained"  >
                    Confirm Loading
            </Button>
            </CardActions>

        </div>
    )
}

WhLoadingFlags.propTypes = {
    className: PropTypes.string
}

export default WhLoadingFlags

