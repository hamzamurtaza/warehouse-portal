import React from 'react'
import WhLoadingFlags from 'views/WhLoadingFlags'

export default function LoadingFlag() {
    return (
        <div>
            <WhLoadingFlags />
        </div>
    )
}