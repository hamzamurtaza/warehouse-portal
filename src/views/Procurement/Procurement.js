import React, { useState, useEffect, Fragment } from 'react'
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import PropTypes, { func } from 'prop-types';
import MuiAlert from '@material-ui/lab/Alert';
import { useHistory } from 'react-router-dom';
import { validateNumeric, validateNumericNoDecimal } from '../../common/validators';
import {
  MuiThemeProvider,
  createMuiTheme,
  TablePagination,
  Dialog,
  Button,
  CardActions,
  Checkbox,
  Card,
  TextField,
  MenuItem,
  FormControl,
  FormHelperText,
  Select,
  InputLabel,
  Snackbar,
  CardContent,
  Grid

} from '@material-ui/core';
import UserModel from '../../models/UserModel'
import MaterialTable from 'material-table';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 800
  },
  statusContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  status: {
    marginRight: theme.spacing(1)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Procurement = (props) => {
  const [entryError, setEntryError] = useState('');
  let history = useHistory();
  const [boolVal, setBoolVal] = useState('')
  const [params, setParams] = useState({
    from: '2020-10-21T06:00',
    to: '2020-10-21T15:00',
  })
  const [batches, setBatches] = useState([{}])
  const [selectedBatch, setSelectedBatch] = useState({});
  const [isLoad, setIsLoad] = useState(false)
  const classes = useStyles();
  const [empty, setEmpty] = useState(false);
  const { className, ...rest } = props;
  var [data, setData] = useState([])
  const [openData, setOpenData] = useState({
    openSuccess: false,
    openError: false
  });
  const columns = [
    { title: 'SKU', field: 'sku', editable: 'never' },
    {
      title: 'Order Quantity',
      field: 'qtty',
      editable: 'never',
      filtering: true
    },
    {
      title: 'Warehouse Quantity',
      field: 'wh_qtty',
      editable: 'never',
      filtering: false
    },
    { title: 'Required Purchase Quantity', field: 'reqd_purchase_qtty', editable: 'never', filtering: false },
    {
      title: 'Fulfilled', field: 'fulfilled', name: 'xyz', render: rowData => <Checkbox disabled={rowData.is_verify} value={rowData.fulfilled} checked={rowData.fulfilled} onChange={async (evt, val) => {
        let newData = { id: rowData.id, batch_id: rowData.batch_id, is_verify: rowData.is_verify, sku_id: rowData.sku_id, sku: rowData.sku, qtty: rowData.qtty, wh_qtty: rowData.wh_qtty, reqd_purchase_qtty: rowData.reqd_purchase_qtty, fulfilled: val, act_purchase: rowData.reqd_purchase_qtty, total_purchase_cost: rowData.total_purchase_cost }
        console.log('new daaata', newData)
        const dataUpdate = [...data];
        const index = rowData.tableData.id;
        dataUpdate[index] = newData;
        setData([...dataUpdate]);
        // rowData.fulfilled = val
        // let dat = {
        //     ...data[rowData.tableData.id],
        //     fulfilled: val
        // };
        // setData(dat)
        //console.log(data)
      }} />, filtering: false
    },
    {
      title: 'Actual Purchased Quantity',
      field: 'act_purchase',
      editable: 'never',
      filtering: false,
      render: rowData => (rowData.fulfilled) ? (<TextField
        value={rowData.reqd_purchase_qtty}
        disabled={true}
        onChange={(evt) => {
          console.log('evt', evt.target.value)
        }}
      />) : (<TextField
        type="number"
        value={rowData.act_purchase}
        disabled={data[rowData.tableData.id].is_verify}
        onChange={async (evt) => {
          if (validateNumericNoDecimal(parseFloat(evt.target.value)) || !evt.target.value) {
            console.log('rowDaata', data[rowData.tableData.id])
            let newData = { id: rowData.id, batch_id: rowData.batch_id, is_verify: rowData.is_verify, sku_id: rowData.sku_id, sku: rowData.sku, qtty: rowData.qtty, wh_qtty: rowData.wh_qtty, reqd_purchase_qtty: rowData.reqd_purchase_qtty, fulfilled: rowData.fulfilled, act_purchase: evt.target.value, total_purchase_cost: rowData.total_purchase_cost }
            const dataUpdate = [...data];
            const index = rowData.tableData.id;
            dataUpdate[index] = newData
            setData([...dataUpdate])
          }
        }}
      />)
    },
    {
      title: 'Total Purchased Cost',
      field: 'total_purchase_cost',
      editable: 'never',
      filtering: false,
      render: rowData =>
        <TextField
          type="number"
          value={rowData.total_purchase_cost}
          disabled={data[rowData.tableData.id].is_verify}
          onChange={async (evt) => {
            console.log(evt.target.value)
            if (validateNumeric(parseFloat(evt.target.value)) || !evt.target.value) {
              let newData = { id: rowData.id, batch_id: rowData.batch_id, is_verify: rowData.is_verify, sku_id: rowData.sku_id, sku: rowData.sku, qtty: rowData.qtty, wh_qtty: rowData.wh_qtty, reqd_purchase_qtty: rowData.reqd_purchase_qtty, fulfilled: rowData.fulfilled, act_purchase: rowData.act_purchase, total_purchase_cost: evt.target.value }
              const dataUpdate = [...data];
              const index = rowData.tableData.id;
              dataUpdate[index] = newData
              setData([...dataUpdate])
            }
          }}
        />
    },
  ]


  useEffect(() => {
    // console.log('slected', selectedBatch.batch_id)
    // UserModel.getInstance().getBatches(
    //   succ => {
    //     console.log('succ batches', succ)
    //     setBatches(succ)
    //   },
    //   err => {
    //     alert(err);
    //     console.log('err batches', err)
    //   }
    // )
  }, [])

  const handleSubmit = async () => {
    // console.log('handle submit', empty)
    // console.log('procurement data', data)
    let isempty = false;
    setIsLoad(true);
    let submitData = [];
    await data.forEach((obj) => {
      if (obj.act_purchase && obj.total_purchase_cost) {
        if (obj.total_purchase_cost == "0" && obj.act_purchase == "0") {
          // console.log("all good")
          // submitData.push({ "price": obj.total_purchase_cost, "sku_id": obj.sku_id, "procure_qty": obj.act_purchase })
        }
        else if (obj.total_purchase_cost != "0" && obj.act_purchase != "0") {
          // console.log("all good2")
          submitData.push({ "price": obj.total_purchase_cost, "sku_id": obj.sku_id, "procure_qty": obj.act_purchase })
        }
        else if (obj.total_purchase_cost != "" && obj.act_purchase == "") {
          // console.log("setting empty", obj)
          isempty = true;
        }
        else if (obj.total_purchase_cost == "" && obj.act_purchase != "") {
          // console.log("setting empty", obj)
          isempty = true;
        }
        if (parseFloat(obj.total_purchase_cost) < 1 || parseFloat(obj.act_purchase) < 1) {
          isempty = true;
        }
      }
      // else {
      //   console.log("setting empty", obj)
      //   isempty = true;
      // }
    })
    if (isempty) {
      alert("Fill all fields correctly")
      setIsLoad(false);
    }
    else {
      UserModel.getInstance().postProcurementForm(
        { items: submitData },
        async succ => {
          setIsLoad(false)
          await setOpenData({ openSuccess: true });
          console.log('succ data', succ)
          window.location.reload();
        },
        err => {
          setIsLoad(false)
          setOpenData({ openError: true, openSuccess: false });
          console.log('err', err)
        }
      )
      console.log('temp arr submit', submitData)
    }
  }

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenData({ openSuccess: false, openError: false });
  };


  const dateHandler = async (event) => {
    console.log('evnt ', event.target.value)
    params[event.target.name] = event.target.value
    await setParams({
      ...params,
      [event.target.name]: event.target.value
    })
    console.log('params ', params)
    let from = params.from.slice(0, 10) + ' ' + params.from.slice(11, 16) + ':00';
    let to = params.to.slice(0, 10) + ' ' + params.to.slice(11, 16) + ':00';

    console.log('from', from)
    console.log('to', to)
  }

  const handleClick = async (event) => {
    let tempArr = []
    UserModel.getInstance().getProcurementForm(
      { "to": params.to, "from": params.from, "status": 0 },
      async data => {
        console.log('procurement data', data)
        await data.forEach(obj => {
          console.log(obj)
          tempArr.push({
            id: obj.sku_id,
            batch_id: obj.batch_id,
            sku_id: obj.sku_id,
            sku: obj.name,
            qtty: obj.order_qty,
            wh_qtty: obj.warehouse_qty,
            reqd_purchase_qtty: (obj.order_qty - obj.warehouse_qty) > 0 ? obj.order_qty - obj.warehouse_qty : 0,
            fulfilled: false,
            is_verify: obj.is_verify_procure,
            is_procure: obj.is_procure,
            act_purchase: obj.procure_qty ? obj.procure_qty : '',
            total_purchase_cost: obj.procure_price ? obj.procure_price : ''
          })
        })
        console.log('temp arr', tempArr)
        setData(tempArr)
        setIsLoad(false)
      },
      err => {
        setIsLoad(false)
        console.log('err', err)
      }
    )
  }

  return (
    <div>
      <div style={{ marginTop: 20 }} >
        <TextField
          id="datetime-local"
          label="Select From"
          type="datetime-local"
          name='from'
          defaultValue={params.from}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={dateHandler}
        />
        <TextField
          id="datetime-local"
          label="Select To"
          type="datetime-local"
          name='to'
          defaultValue={params.to}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          style={{ marginLeft: 40 }}
          onChange={dateHandler}
        />
      </div>
      <Button style={{ margin: 10 }} onClick={handleClick}>Submit</Button>
      <br />
      <br />
      <div>Note: Rows with Purchased Quantity and Cost 0 or Null will be omitted from submission.</div>
      <Card>
        {/* <div>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-helper-label">Batch</InputLabel>
            <Select
              labelId="demo-simple-select-helper-label"
              id="demo-simple-select-helper"

              value={selectedBatch}
              onChange={handleChange}
            >
              {
                batches.map((val) => {
                  return <MenuItem value={val}>{val.batch_name}</MenuItem>
                })
              }
            </Select>
            <FormHelperText style={{ margin: 10 }}>Select a Batch Id</FormHelperText>
          </FormControl>
        </div> */}
        <MaterialTable
          title="Orders"
          columns={columns}
          data={data}
          isLoading={isLoad}
          className={clsx(classes.root, className)}
          // onFilterChange={filterChange}
          actions={[
            {
              icon: 'add',
              tooltip: 'Adhoc Procurement',
              isFreeAction: true,
              onClick: () => {
                history.push('/adhoc-procurement');
              }
            }
          ]}
          options={{
            paging: false,
            exportButton: true,
            filtering: true,
            // selection: true,

          }}
        ></MaterialTable>

        <CardContent>
          <Snackbar
            open={openData.openSuccess}
            autoHideDuration={3000}
            onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
              Procurement Data Added Successfully
              </Alert>
          </Snackbar>
          <Snackbar
            open={openData.openError}
            autoHideDuration={3000}
            onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              Error while Submitting Data
              </Alert>
          </Snackbar>
        </CardContent>

      </Card>

      <CardActions>
        <Button color="primary" variant="contained" onClick={handleSubmit} >
          Confirm Procurement
            </Button>
      </CardActions>

    </div>
  )
}

Procurement.propTypes = {
  className: PropTypes.string
}

export default Procurement
