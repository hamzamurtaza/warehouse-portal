import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { useHistory } from 'react-router-dom';
import {
  MuiThemeProvider,
  createMuiTheme,
  TablePagination,
  TextField,
  Button,
} from '@material-ui/core';
import MaterialTable from 'material-table';
import UserModel from 'models/UserModel';
import { validateNumeric, validateNumericNoDecimal } from '../../common/validators';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 800
  },
  statusContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  status: {
    marginRight: theme.spacing(1)
  },
  actions: {
    justifyContent: 'flex-end'
  }
}));


const SetStockQuantity = props => {

  const [params, setParams] = useState({
    page: 1,
    offset: 0,
    cityId: 1,
    from: '2020-10-21T06:00',
    to: '2020-10-21T15:00'
  });
  const [skuData, setSkuData] = useState([])
  const [sendingData, setSendingData] = useState([]);
  var [isLoader, setIsLoader] = useState(false);
  const [searchDataForPagination, setSearchDataForPagination] = useState([]);
  const { className, ...rest } = props;
  const [searchText, setSearchText] = useState('');
  const [change, setChange] = useState('false')
  // const [data] = useState(mockData);
  const classes = useStyles();
  let history = useHistory();

  const [columns, setColumn] = useState([
    {
      title: 'Sku',
      field: 'name',
      editable: 'never',
    },
    {
      title: 'Procured Quantity',
      field: 'proc_qty',
      filtering: false,
    },
    {
      title: 'Cost',
      field: 'cost',
      filtering: false,
    }
  ]);

  useEffect(() => {
    UserModel.getInstance().getSku(
      null,
      async (succ) => {
        console.log(succ)
        setSkuData(succ)
      },
      (err) => {
        console.log(err)
      }
    )
  }, []);

  const theme = createMuiTheme({
    typography: {
      fontFamily: 'Nunito Sans, Roboto, sans-serif'
    }
  });

  const handleNextPage = () => {
    window.confirm("Unsaved changes will be lost. Press UPDATE WAREHOUSE to submit changes. Are you sure you want to change page?")
    const newOffset = params.offset + 20;
    const paramObj = { offset: newOffset };
    const newPage = params.page + 1;

    if (searchText === '') {
      setIsLoader(true);
      UserModel.getInstance().getSku(
        paramObj,
        async data => {
          setSkuData(data);
          setParams({ ...params, offset: newOffset, page: newPage });
          setIsLoader(false);
        },
        err => {
          setIsLoader(false);
          // console.log(err);
        }
      );
    } else {
      UserModel.getInstance().globalSearchProductSku(
        { "product_sku.name": searchText },
        paramObj,
        async data => {
          // console.log('data search', data);
          let tempArr = [];
          setSkuData(data)
          setParams({ ...params, offset: newOffset, page: newPage });
        },
        err => {
          // console.log(err)
        }
      )
    }
  };

  const handlePreviousPage = () => {
    if (params.offset > 0) {
      window.confirm("Unsaved changes will be lost. Press UPDATE WAREHOUSE to submit changes. Are you sure you want to change page?")
      const newOffset = params.offset - 20;
      const newPage = params.page - 1;
      const paramObj = { offset: newOffset };
      setChange('false')
      if (searchText === '') {
        // setIsLoader(true);
        UserModel.getInstance().getSku(
          paramObj,
          async data => {

            setSkuData(data);
            setParams({ ...params, offset: newOffset, page: newPage });
            // setIsLoader(false);
          },
          err => {
            // setIsLoader(false);
            console.log(err);
          }
        );
      } else {
        UserModel.getInstance().globalSearchProductSku(
          { "product_sku.name": searchText },
          paramObj,
          async data => {
            // console.log(prodData)
            setSkuData(data);
            setParams({ ...params, offset: newOffset, page: newPage });
          },
          err => {
            console.log(err)
          }
        )
      }
    }
  };

  // const handleChange = event => {

  // };

  const filterChange = data => {
    console.log("dete", data)
    if (data.length > 0 && data[0].value.length > 0) {
      setChange('true')
      setSearchText(data[0].value);
      UserModel.getInstance().globalSearchProductSku(
        { "product_sku.name": data[0].value },
        null,
        (succ) => {
          setSkuData(succ)
          console.log(succ)
        },
        (err) => {
          console.log(err)
        }
      )
    } else {
      UserModel.getInstance().getSku(
        null,
        async (succ) => {
          console.log(succ)
          setSkuData(succ)
        },
        (err) => {
          console.log(err)
        }
      )
    }
  };

  const updateWh = () => {
    // console.log(skuData)
    let tempArr = []
    if (sendingData.length > 0) {
      sendingData.forEach((skuRow) => {
        tempArr.push({ "sku_id": skuRow.id, "procure_qty": skuRow.proc_qty, "price": skuRow.cost })
      })
      console.log(tempArr)
      UserModel.getInstance().setStock(
        { "items": tempArr },
        null,
        (succ) => {
          console.log(succ)
          window.location.reload();
        },
        (err) => {
          console.log(err)
          alert("Error while making procurement")
        }
      )
    }
    else {
      alert("Please select an SKU to add")
    }
  }

  return (
    <div>
      <MuiThemeProvider theme={theme}>
        <MaterialTable
          title="Adhoc Procurement"
          columns={columns}
          data={skuData}
          onFilterChange={filterChange}
          editable={{
            onRowUpdate: (newData, oldData) =>
              new Promise((resolve, reject) => {
                setTimeout(() => {
                  console.log(newData)
                  const dataUpdate = [...skuData];
                  const index = oldData.tableData.id;
                  dataUpdate[index] = newData;
                  if (newData.proc_qty === "" || newData.cost === "" || newData.proc_qty === "0" || newData.cost === "0" || !newData.proc_qty || !newData.cost) {
                    alert("invalid entry")
                  }
                  else {
                    if (validateNumericNoDecimal(parseFloat(newData.proc_qty)) && validateNumeric(parseFloat(newData.cost))) {
                      setSendingData([...sendingData, newData])
                      setSkuData([...dataUpdate]);
                    }
                    else {
                      alert("invalid entry")
                    }
                  }
                  resolve();
                }, 1000)
              }),
          }}
          options={{
            paging: false,
            search: false,
            // selection: true,
            filtering: true
          }}
        />
        <TablePagination
          component="div"
          rowsPerPage={5}
          count={-1}
          rowsPerPageOptions={[5]}
          page={params.page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
            onClick: handlePreviousPage
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
            onClick: handleNextPage
          }}
          labelDisplayedRows={() => {
            return `Page ${params.page}`;
          }}
        />
      </MuiThemeProvider >
      <Button onClick={updateWh} color="primary" variant="contained">Update Warehouse</Button>
    </div>
  );
};

SetStockQuantity.propTypes = {
  className: PropTypes.string
};

export default SetStockQuantity;