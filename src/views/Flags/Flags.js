import { Grid } from '@material-ui/core'
import React from 'react'
import FlagTile from '../Flags/components/FlagTile'

export default function Flags() {
    return (
        <div>
            <Grid container spacing={4}>
                <Grid item
                    lg={3}
                    sm={6}
                    xl={3}
                    xs={12}>
                    <FlagTile name="PROCUREMENT" />
                </Grid>
                <Grid item
                    lg={3}
                    sm={6}
                    xl={3}
                    xs={12}>
                    <FlagTile name="PICKING" />
                </Grid>
                {/* <Grid item
                    lg={3}
                    sm={6}
                    xl={3}
                    xs={12}>
                    <FlagTile name="SORTING" />
                </Grid>
                <Grid item
                    lg={3}
                    sm={6}
                    xl={3}
                    xs={12}>
                    <FlagTile name="LOADING" />
                </Grid> */}
                <Grid item
                    lg={3}
                    sm={6}
                    xl={3}
                    xs={12}>
                    <FlagTile name="BATCHCREATE" />
                </Grid>
                <Grid item
                    lg={3}
                    sm={6}
                    xl={3}
                    xs={12}>
                    <FlagTile name="RETURNS" />
                </Grid>
            </Grid>
        </div>
    )
}
