// import React, { useState, useEffect, Component } from 'react';
// import { Link as RouterLink, withRouter } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import validate from 'validate.js';
// import { validatePhone } from '../../common/validators'
// import { makeStyles } from '@material-ui/styles';
// import {
//   Grid,
//   Button,
//   IconButton,
//   TextField,
//   Link,
//   Typography,
//   CircularProgress,
//   FormControlLabel,
//   FormControl,
//   Radio,
//   RadioGroup,
//   FormLabel

// } from '@material-ui/core';
// import ArrowBackIcon from '@material-ui/icons/ArrowBack';
// import UserModel from '../../models/UserModel';

// // export default class SignIn extends Component {
// //   componentDidMount() {}

// //   onClick(e) {
// //     UserModel.getInstance().Login(
// //       'admin@example.com',
// //       '112112112',
// //       function(data) {
// //         console.log('data', data);
// //       },
// //       function(err) {
// //         console.log('error', err);
// //       }
// //     );
// //     alert('button click');
// //   }

// //   render() {
// //     return (
// //       <React.Fragment>
// //         <Button onClick={this.onClick}>Sigin</Button>
// //       </React.Fragment>
// //     );
// //   }
// // }

// const schema = {
//   mobile: {
//     presence: { allowEmpty: false, message: 'is required' },
//     length: {
//       maximum: 13,
//       minimum: 10

//     }
//   },
//   password: {
//     presence: { allowEmpty: false, message: 'is required' },
//     length: {
//       maximum: 128,
//       minimum: 6
//     }
//   }
// };


// const useStyles = makeStyles(theme => ({
//   root: {
//     backgroundColor: theme.palette.background.default,
//     height: '100%'
//   },
//   colorPrimary: {
//     color: "white"
//   },
//   grid: {
//     height: '100%'
//   },
//   name: {
//     marginTop: theme.spacing(3),
//     color: theme.palette.white
//   },
//   // bio: {
//   //   color: theme.palette.white
//   // },
//   contentContainer: {},
//   content: {
//     height: '100%',
//     display: 'flex',
//     flexDirection: 'column'
//   },
//   contentHeader: {
//     display: 'flex',
//     alignItems: 'center',
//     paddingTop: theme.spacing(5),
//     paddingBototm: theme.spacing(2),
//     paddingLeft: theme.spacing(2),
//     paddingRight: theme.spacing(2)
//   },
//   logoImage: {
//     marginLeft: theme.spacing(4)
//   },
//   contentBody: {
//     flexGrow: 1,
//     display: 'flex',
//     alignItems: 'center',
//     [theme.breakpoints.down('md')]: {
//       justifyContent: 'center'
//     }
//   },
//   form: {
//     paddingLeft: 100,
//     paddingRight: 100,
//     paddingBottom: 125,
//     flexBasis: 700,
//     [theme.breakpoints.down('sm')]: {
//       paddingLeft: theme.spacing(2),
//       paddingRight: theme.spacing(2)
//     }
//   },
//   title: {
//     marginTop: theme.spacing(3),
//     fontWeight: 'bold',
//     marginBottom: 30
//   },
//   // socialButtons: {
//   //   marginTop: theme.spacing(3)
//   // },
//   socialIcon: {
//     marginRight: theme.spacing(1)
//   },
//   sugestion: {
//     marginTop: theme.spacing(2)
//   },
//   textField: {
//     marginTop: theme.spacing(2)
//   },
//   signInButton: {
//     margin: theme.spacing(2, 0)
//   }
// }));

// const SignIn = props => {
//   const { history } = props;

//   const classes = useStyles();
//   const [invalidCredentials] = useState({ boolean: false });
//   var [invalidAuthText] = useState('');
//   const [isLoader, setIsLoader] = useState(false);
//   const [value, setValue] = React.useState('manage');

//   const [formState, setFormState] = useState({
//     isValid: false,
//     values: { mobileCode: '+92', mobile: '' },
//     touched: {},
//     errors: {}
//   });

//   useEffect(() => {
//     const errors = validate(formState.values, schema);

//     setFormState(formState => ({
//       ...formState,
//       isValid: errors ? false : true,
//       errors: errors || {}
//     }));
//   }, [formState.values]);

//   const handleBack = () => {
//     history.goBack();
//   };

//   const handleRole = (event, val) => {
//     setValue(val)
//   }

//   const handleChange = event => {
//     event.persist();
//     // console.log('evnt',event.target)
//     setFormState(formState => ({
//       ...formState,
//       values: {
//         ...formState.values,
//         [event.target.name]:
//           event.target.type === 'checkbox'
//             ? event.target.checked
//             : ((event.target.name == 'mobile') ?
//               ((validatePhone(event.target.value)) ? event.target.value :
//                 (formState.values.mobile.length < 2 ? '' :
//                   formState.values.mobile)) :
//               event.target.value
//             )
//       },
//       touched: {
//         ...formState.touched,
//         [event.target.name]: true
//       }
//     }));
//   };

//   const handleSignIn = event => {
//     event.preventDefault();
//     setIsLoader(true);
//     // console.log('form state', formState.values.password)
//     // console.log(`${formState.values.mobileCode}${formState.values.mobile}`)
//     console.log('radio', formState.values.mobile[0])
//     if (formState.values.mobile[0] != 0) {
//       UserModel.getInstance().Login(
//         `${formState.values.mobile}`,
//         formState.values.password,
//         value,
//         data => {
//           console.log('Login data', data);
//           setIsLoader(false)
//           history.push('/dashboard');
//         },
//         err => {
//           console.log(err);
//           invalidCredentials.boolean = true;
//           invalidAuthText = err;
//           alert(invalidAuthText);
//           setIsLoader(false)
//         }
//       );
//       //history.push('/');
//     }
//     else {
//       alert('Please provide valid mobile number. Example: 3214248868')
//     }


//   };

//   const hasError = field =>
//     formState.touched[field] && formState.errors[field] ? true : false;

//   return (
//     <div className={classes.root}>
//       <Grid className={classes.grid} container>
//         <Grid className={classes.content} item lg={7} xs={12}>
//           <div className={classes.content}>
//             <div className={classes.contentBody}>
//               <form className={classes.form} onSubmit={handleSignIn}>
//                 <Typography className={classes.title} variant="h2">
//                   SIGN IN AS
//                 </Typography>
//                 <FormControl component="fieldset">
//                   <RadioGroup aria-label="role" name="role" value={value} onChange={handleRole}>
//                     <FormControlLabel value="manage" control={<Radio />} label="Manager" />
//                     <FormControlLabel value="procurement" control={<Radio />} label="Procurement" />

//                   </RadioGroup>
//                 </FormControl>
//                 {/* <Typography color="textSecondary" gutterBottom>
//                   Sign in with social media
//                 </Typography> */}
//                 {/* <Grid className={classes.socialButtons} container spacing={2}>
//                   <Grid item>
//                     <Button
//                       color="primary"
//                       onClick={handleSignIn}
//                       size="large"
//                       variant="contained">
//                       <FacebookIcon className={classes.socialIcon} />
//                       Login with Facebook
//                     </Button>
//                   </Grid>
//                   <Grid item>
//                     <Button
//                       onClick={handleSignIn}
//                       size="large"
//                       variant="contained">
//                       <GoogleIcon className={classes.socialIcon} />
//                       Login with Google
//                     </Button>
//                   </Grid>
//                 </Grid> */}
//                 {/* <Typography
//                   align="center"
//                   className={classes.sugestion}
//                   color="textSecondary"
//                   variant="body1">
//                   or login with email address
//                 </Typography> */}

//                 <TextField
//                   className={classes.textField}
//                   error={hasError('mobile')}
//                   fullWidth
//                   helperText={
//                     hasError('mobile') ? formState.errors.mobile[0] : null
//                   }
//                   label="+92"
//                   placeholder="3352493858"
//                   name="mobile"
//                   onChange={handleChange}
//                   type="numeric"
//                   value={formState.values.mobile}
//                   variant="outlined"
//                 />

//                 <TextField
//                   className={classes.textField}
//                   error={hasError('password')}
//                   fullWidth
//                   helperText={
//                     hasError('password') ? formState.errors.password[0] : null
//                   }
//                   label="Password"
//                   name="password"
//                   onChange={handleChange}
//                   type="password"
//                   value={formState.values.password || ''}
//                   variant="outlined"
//                 />
//                 <Button
//                   className={classes.signInButton}
//                   color="primary"
//                   disabled={!formState.isValid}
//                   fullWidth
//                   size="large"
//                   type="submit"
//                   variant="contained">
//                   {isLoader ? <CircularProgress size={25} className={classes.colorPrimary} /> : 'Sign in now'}
//                 </Button>
//                 {/* <Typography color="textSecondary" variant="body1">
//                   Don't have an account?{' '}
//                   <Link component={RouterLink} to="/sign-up" variant="h6">
//                     Sign up
//                   </Link>
//                 </Typography> */}
//               </form>
//             </div>
//           </div>
//         </Grid>
//       </Grid>
//     </div>
//   );
// };

// SignIn.propTypes = {
//   history: PropTypes.object
// };

// export default withRouter(SignIn);

import React, { useState, useEffect, Component } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { validatePhone } from '../../common/validators'
import { makeStyles } from '@material-ui/styles';
import {
  Grid,
  Button,
  IconButton,
  TextField,
  Link,
  Typography,
  CircularProgress,
  FormControlLabel,
  FormControl,
  Radio,
  RadioGroup,
  FormLabel

} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import UserModel from '../../models/UserModel';

// export default class SignIn extends Component {
//   componentDidMount() {}

//   onClick(e) {
//     UserModel.getInstance().Login(
//       'admin@example.com',
//       '112112112',
//       function(data) {
//         console.log('data', data);
//       },
//       function(err) {
//         console.log('error', err);
//       }
//     );
//     alert('button click');
//   }

//   render() {
//     return (
//       <React.Fragment>
//         <Button onClick={this.onClick}>Sigin</Button>
//       </React.Fragment>
//     );
//   }
// }

const schema = {
  mobile: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 11,
      minimum: 10

    }
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128,
      minimum: 6
    }
  }
};


const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.default,
    height: '100%'
  },
  colorPrimary: {
    color: "white"
  },
  grid: {
    height: '100%'
  },
  name: {
    marginTop: theme.spacing(3),
    color: theme.palette.white
  },
  // bio: {
  //   color: theme.palette.white
  // },
  contentContainer: {},
  content: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  contentHeader: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: theme.spacing(5),
    paddingBototm: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  },
  logoImage: {
    marginLeft: theme.spacing(4)
  },
  contentBody: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center'
    }
  },
  form: {
    paddingLeft: 100,
    paddingRight: 100,
    paddingBottom: 125,
    flexBasis: 700,
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    }
  },
  title: {
    marginTop: theme.spacing(3),
    fontWeight: 'bold',
    marginBottom: 30
  },
  // socialButtons: {
  //   marginTop: theme.spacing(3)
  // },
  socialIcon: {
    marginRight: theme.spacing(1)
  },
  sugestion: {
    marginTop: theme.spacing(2)
  },
  textField: {
    marginTop: theme.spacing(2)
  },
  signInButton: {
    margin: theme.spacing(2, 0)
  }
}));

const SignIn = props => {
  const { history } = props;

  const classes = useStyles();
  const [invalidCredentials] = useState({ boolean: false });
  var [invalidAuthText] = useState('');
  const [isLoader, setIsLoader] = useState(false);
  const [value, setValue] = React.useState('manage');

  const [formState, setFormState] = useState({
    isValid: false,
    values: { mobileCode: '+92', mobile: '' },
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleBack = () => {
    history.goBack();
  };

  const handleRole = (event, val) => {
    setValue(val)
  }

  const handleChange = event => {
    event.persist();
    // console.log('evnt',event.target)
    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : ((event.target.name == 'mobile') ? ((validatePhone(event.target.value)) ? event.target.value : (formState.values.mobile.length < 2 ? '' : formState.values.mobile)) : event.target.value)
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleSignIn = event => {
    event.preventDefault();
    setIsLoader(true);
    console.log('form state', formState.values.password)
    console.log(`${formState.values.mobileCode}${formState.values.mobile}`)
    console.log('radio', value)
    if (formState.values.mobile[0] != 0) {
      UserModel.getInstance().Login(
        `${formState.values.mobileCode}${formState.values.mobile}`,
        formState.values.password,
        value,
        data => {
          console.log('Login data', data);
          setIsLoader(false)
          history.push('/dashboard');
        },
        err => {
          console.log(err);
          invalidCredentials.boolean = true;
          invalidAuthText = err;
          alert(invalidAuthText);
          setIsLoader(false)
        }
      );
      //history.push('/');
    }
    else {
      alert('Please provide valid mobile number. Example: 3214248868')
    }


  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <div className={classes.root}>
      <Grid className={classes.grid} container>
        <Grid className={classes.content} item lg={7} xs={12}>
          <div className={classes.content}>
            <div className={classes.contentBody}>
              <form className={classes.form} onSubmit={handleSignIn}>
                <Typography className={classes.title} variant="h2">
                  SIGN IN AS
                </Typography>
                <FormControl component="fieldset">
                  <RadioGroup aria-label="role" name="role" value={value} onChange={handleRole}>
                    <FormControlLabel value="manage" control={<Radio />} label="Manager" />
                    <FormControlLabel value="procurement" control={<Radio />} label="Procurement" />

                  </RadioGroup>
                </FormControl>
                {/* <Typography color="textSecondary" gutterBottom>
                  Sign in with social media
                </Typography> */}
                {/* <Grid className={classes.socialButtons} container spacing={2}>
                  <Grid item>
                    <Button
                      color="primary"
                      onClick={handleSignIn}
                      size="large"
                      variant="contained">
                      <FacebookIcon className={classes.socialIcon} />
                      Login with Facebook
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      onClick={handleSignIn}
                      size="large"
                      variant="contained">
                      <GoogleIcon className={classes.socialIcon} />
                      Login with Google
                    </Button>
                  </Grid>
                </Grid> */}
                {/* <Typography
                  align="center"
                  className={classes.sugestion}
                  color="textSecondary"
                  variant="body1">
                  or login with email address
                </Typography> */}

                <TextField
                  className={classes.textField}
                  error={hasError('mobile')}
                  fullWidth
                  helperText={
                    hasError('mobile') ? formState.errors.mobile[0] : null
                  }
                  label="+92"
                  placeholder="3352493858"
                  name="mobile"
                  onChange={handleChange}
                  type="numeric"
                  value={formState.values.mobile}
                  variant="outlined"
                />

                <TextField
                  className={classes.textField}
                  error={hasError('password')}
                  fullWidth
                  helperText={
                    hasError('password') ? formState.errors.password[0] : null
                  }
                  label="Password"
                  name="password"
                  onChange={handleChange}
                  type="password"
                  value={formState.values.password || ''}
                  variant="outlined"
                />
                <Button
                  className={classes.signInButton}
                  color="primary"
                  disabled={!formState.isValid}
                  fullWidth
                  size="large"
                  type="submit"
                  variant="contained">
                  {isLoader ? <CircularProgress size={25} className={classes.colorPrimary} /> : 'Sign in now'}
                </Button>
                {/* <Typography color="textSecondary" variant="body1">
                  Don't have an account?{' '}
                  <Link component={RouterLink} to="/sign-up" variant="h6">
                    Sign up
                  </Link>
                </Typography> */}
              </form>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

SignIn.propTypes = {
  history: PropTypes.object
};

export default withRouter(SignIn);
