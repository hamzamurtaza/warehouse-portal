const checked = (value, options) => {
  if (value !== true) {
    return options.message || 'must be checked';
  }
};
export const validatePhone = (text) => {
  // var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{1,6}$/im
  var re = /^[0-9]{1,30}$/
  return (re.test(text))
};

export const validateNumeric = (text) => {
  var re = /^(\d*\.?\d[^a-zA-Z]*)$/
  return (re.test(text))
}

export const validateNumericNoDecimal = (text) => {
  var re = /^\d{1,10}$/
  return (re.test(text))
}

export default {
  checked
};
